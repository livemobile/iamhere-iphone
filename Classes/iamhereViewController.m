
#import "iamhereViewController.h"
#import  <QuartzCore/QuartzCore.h> 
#import "GradientView.h"
#import "MapPin.h"
#import "GestureRecognizer.h"
#import "GNGeoHash.h"

@implementation iamhereViewController

@synthesize mapView;
@synthesize placesTable;

#define kVerySmallValue (0.000001)

CLLocationManager *locationManager;
CLLocation *location;
MapPin *mapPin;
NSMutableArray *mapAnnotations;
NSString *currentPlaceName;
CLLocationCoordinate2D currentCoordinates;
CLLocationCoordinate2D userLoc;
NSString *messageBody;
CGPoint touchPoint;
BOOL trackUser = true;
UITextField *titleField;
NSString *titleText;



// Implement loadView if you want to create a view hierarchy programmatically
- (void)loadView {
		[super loadView];
	//Setup background gradient
	/*
	GradientView *myView = (GradientView *)self.view;	
	[myView setColoursWithCGColors:[UIColor colorWithRed:0.9 green:0.8 blue:0.2 alpha:1.0].CGColor:[UIColor colorWithRed:0.9 green:0.5 blue:0.1 alpha:1.0].CGColor];
	*/
	
	/************************
	 Setup location services
	 ************************/
	locationManager = [[[CLLocationManager alloc] init]autorelease];
	location = [[[CLLocation alloc] init]autorelease];
	
	locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
	locationManager.delegate = self;
	[locationManager startUpdatingLocation];
	location = [locationManager location];
	currentCoordinates =  location.coordinate;
	[locationManager stopUpdatingLocation];
	
	/*************
	 setup mapView
	 *************/
	//Map movement recogniser
	GestureRecognizer * tapInterceptor = [[GestureRecognizer alloc] init];
	tapInterceptor.touchesBeganCallback = ^(NSSet * touches, UIEvent * event) {
		trackUser = false;
		NSLog(@"Swipe Detected");
	};
	[mapView addGestureRecognizer:tapInterceptor];
	[tapInterceptor release];
	
	//Map long press recogniser
	UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] 
										  initWithTarget:self action:@selector(handleLongPress:)];
	lpgr.minimumPressDuration = 0.4; //user needs to press for 0.4 of a seconds
	[mapView addGestureRecognizer:lpgr];
	[lpgr release];
	
	//Plot saved places onto map
	[self getUserPlaces];
    mapView.showsUserLocation = YES;
    mapView.delegate = self;
	
	/******************
	 Setup places table
	 ******************/
	placesTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 420) style:UITableViewStylePlain];
	[placesTable setAutoresizesSubviews:YES];
	[placesTable setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
	[placesTable setDataSource:self];
	[placesTable setDelegate:self];

	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 44)] autorelease];
	label.backgroundColor = [UIColor lightGrayColor];
	label.font = [UIFont boldSystemFontOfSize:18];
	label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.8];
	label.textAlignment = UITextAlignmentCenter;
	label.textColor = [UIColor whiteColor];
	label.text = NSLocalizedString(@"MYPLACES", nil);
	label.layer.borderColor = [UIColor darkGrayColor].CGColor;
	label.layer.borderWidth = 2.0;
	
	placesTable.tableHeaderView = label;
	placesTable.alpha = 0.0;
	
	[[self view] addSubview:placesTable];
}

 //Implement viewDidLoad if you need to do additional setup after loading the view.
- (void)viewDidLoad {
	[super viewDidLoad];
}

/***************************
 MapView Delegate Functions
***************************/

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
	
	userLoc.latitude = aUserLocation.coordinate.latitude;
    userLoc.longitude = aUserLocation.coordinate.longitude;
	if(trackUser)
	{
		[self updateMapWithLocation:userLoc];
	}
}

- (void)updateMapWithLocation:(CLLocationCoordinate2D) newlocation
{
	if([self locationEqual:newlocation.longitude isEqualTo:-180.00000000] || [self locationEqual:newlocation.latitude isEqualTo:0.000000])
	{//the location services has been turned off
		UIAlertView *locErrorAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"LOCDISABLED", nil)
																message:NSLocalizedString(@"TOENABLE", nil)
															   delegate:nil 
													  cancelButtonTitle:NSLocalizedString(@"OK", nil)
													  otherButtonTitles:nil];
		[locErrorAlert show];
		[locErrorAlert release];
		return;	
	}
	
	MKCoordinateRegion region;
	MKCoordinateSpan span;
	span.latitudeDelta = 0.003;
	span.longitudeDelta = 0.003;
	region.span = span;
	region.center = newlocation;
	[mapView setRegion:region animated:YES];
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
	// if it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
	// try to dequeue an existing pin view first
	static NSString* AnnotationIdentifier = @"AnnotationIdentifier";
	MKPinAnnotationView* pinView = [[[MKPinAnnotationView alloc]
									 initWithAnnotation:annotation reuseIdentifier:AnnotationIdentifier] autorelease];
	pinView.animatesDrop=YES;
	pinView.canShowCallout=YES;
	pinView.pinColor=MKPinAnnotationColorRed;
	
	
	UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
	UIImage *sendImage = [UIImage imageNamed:@"send.png"];
	[rightButton setImage:sendImage forState:UIControlStateNormal];
	[rightButton addTarget:self
					action:@selector(sendPlace)
		  forControlEvents:UIControlEventTouchUpInside];
	pinView.rightCalloutAccessoryView = rightButton;
	
	UIButton* leftButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
	UIImage *btnImage = [UIImage imageNamed:@"bin.png"];
	[leftButton setImage:btnImage forState:UIControlStateNormal];
	[leftButton addTarget:self
				   action:@selector(deletePlaceOnMap)
		 forControlEvents:UIControlEventTouchUpInside];
	pinView.leftCalloutAccessoryView = leftButton;
	
	[sendImage release];	
	[btnImage release];
	
	return pinView;
}


- (void)mapView:(MKMapView *)mapViewInput didSelectAnnotationView:(MKAnnotationView *)view
{
	trackUser = false;
	for(MapPin *object in mapViewInput.selectedAnnotations){
        NSLog(@"%@", object.title);
		mapPin = object;
		[mapPin retain];
		currentPlaceName = object.title;
		currentCoordinates = object.coordinate;
	}
}


/********************************
 Functions performed on MapView
********************************/

- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
	[self showPlaceNameEntryAlert];
	touchPoint = [gestureRecognizer locationInView:mapView];  
	
}

- (void)showPlaceNameEntryAlert {
	
	UIAlertView *titleAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"PLACENAME", nil) 
														message:@"\n\n\n"
														delegate:self 
														cancelButtonTitle:NSLocalizedString(@"CANCEL",nil) 
														otherButtonTitles:NSLocalizedString(@"OK",nil), nil];
	
	titleField = [[UITextField alloc] initWithFrame:CGRectMake(16,65,252,25)];
	titleField.font = [UIFont systemFontOfSize:18];
	titleField.backgroundColor = [UIColor whiteColor];
	titleField.keyboardAppearance = UIKeyboardAppearanceAlert;
	titleField.delegate = self;
	[titleField becomeFirstResponder];
	[titleAlert addSubview:titleField];
	
	[titleAlert setTransform:CGAffineTransformMakeTranslation(0,0)];
	[titleAlert show];
	[titleAlert release];
	[titleField release];
}

- (void)textFieldDidEndEditing:(UITextField *)text {
	titleText = [NSString stringWithFormat:@"%@", text.text]; 
	NSLog(@"Title text: %@",titleText);
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
		NSLog(@"creating point");
		CLLocationCoordinate2D touchMapCoordinate = 
		[mapView convertPoint:touchPoint toCoordinateFromView:mapView];
		[self plotPin:touchMapCoordinate];
    }
}


-(IBAction)plotPin:(CLLocationCoordinate2D) point
{	
	trackUser = false;
	MapPin *ann = [[MapPin alloc] init]; 
	ann.title = titleText;
	ann.subtitle = NSLocalizedString(@"SENDLOC", nil);
	ann.coordinate = point; 
	mapPin = ann;
	[self storeUsersPlace:ann];
	[mapView addAnnotation:ann];
	[mapView selectAnnotation:ann animated:YES];
	[ann release];
}


-(IBAction)trackUserLocation
{
	trackUser = true;
	[self updateMapWithLocation:userLoc];
}


-(void)moveToLocation:(MapPin *)newLocation
{
	trackUser = false;
	MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.001;
    span.longitudeDelta = 0.001;
    region.span = span;
    region.center = newLocation.coordinate;
    [mapView setRegion:region animated:YES];
	[mapView selectAnnotation:newLocation animated:YES];
	mapPin  = newLocation;
}


/****************************************
 Functions to send location sms messages
 ***************************************/

-(IBAction)sendIamhere 
{
	
	if([self locationEqual:userLoc.latitude isEqualTo:-180.00000000] || [self locationEqual:userLoc.longitude isEqualTo:-0.000000])
	{//the location services has been turned off
		UIAlertView *locErrorAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"LOCDISABLED", nil)
																message:NSLocalizedString(@"TOENABLE", nil)
															   delegate:nil 
													  cancelButtonTitle:NSLocalizedString(@"OK", nil)
													  otherButtonTitles:nil];
		[locErrorAlert show];
		[locErrorAlert release];
		return;	
	}
	
	GNGeoHash *geoHash = [GNGeoHash withCharacterPrecision:userLoc.latitude andLongitude:userLoc.longitude andNumberOfCharacters:24];

	messageBody = [NSString stringWithFormat:@"I am here Map: www.iamhe.re/%@",[geoHash toBase32]]; 
	NSLog(@"Location points: %@ ", messageBody); 
	
	MFMessageComposeViewController *picker = [[[MFMessageComposeViewController alloc] init] autorelease];
	picker.messageComposeDelegate = self;
	
	//picker.recipients = [NSArray arrayWithObject:number.text];
	picker.body = messageBody;
	
	dispatch_async(dispatch_get_main_queue(), ^{
		[self presentModalViewController:picker animated:NO];    
	});
}

-(void)sendPlace
{	
	GNGeoHash *geoHash = [GNGeoHash withCharacterPrecision:userLoc.latitude andLongitude:userLoc.longitude andNumberOfCharacters:24];
	
	messageBody = [NSString stringWithFormat:@"%@ Map: www.iamhe.re/%@", currentPlaceName, [geoHash toBase32]]; 
	NSLog(@"Location points: %@ ", messageBody); 
	
	MFMessageComposeViewController *picker = [[[MFMessageComposeViewController alloc] init] autorelease];
	picker.messageComposeDelegate = self;
	
	//picker.recipients = [NSArray arrayWithObject:number.text];
	picker.body = messageBody;
	dispatch_async(dispatch_get_main_queue(), ^{
		[self presentModalViewController:picker animated:NO];  
	});
	
	
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
	
	switch (result)
	{
		case MessageComposeResultCancelled:
			break;
		case MessageComposeResultSent:
			NSLog(@"Result: sent");
			UIAlertView *sentAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"IAMHERE", nil)
																  message:NSLocalizedString(@"MESSAGESENT", nil)
																 delegate:self
														cancelButtonTitle:@"Ok"
														otherButtonTitles:nil];
			[sentAlert show];
			[sentAlert release];
			break;
		case MessageComposeResultFailed:
			NSLog(@"Result: failed");
			UIAlertView *failedAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"IAMHERE", nil)
																  message:NSLocalizedString(@"MESSAGEFAILED", nil)
																 delegate:self
														cancelButtonTitle:@"Ok"
														otherButtonTitles:nil];
			[failedAlert show];
			[failedAlert release];
			break;
		default:
			NSLog(@"Result: not sent");
			UIAlertView *notSentAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"IAMHERE", nil)
																   message:NSLocalizedString(@"MESSAGENOTSENT", nil)
																  delegate:self
														 cancelButtonTitle:@"Ok"
														 otherButtonTitles:nil];
			[notSentAlert show];
			[notSentAlert release];
			break;
	}
	
	[self dismissModalViewControllerAnimated:YES];
	
}


/***************************************
 Functions to persist locations points
 **************************************/

-(void)storeUsersPlace:(MapPin*) theAnnotation
{	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	
	NSString *title = [NSString stringWithFormat:@"%@", theAnnotation.title];
	NSString *subtitle = [NSString stringWithFormat:@"%@", theAnnotation.subtitle];
    NSString *latitude = [NSString stringWithFormat:@"%.06f", theAnnotation.coordinate.latitude];//latitude is a double here
    NSString *longitude = [NSString stringWithFormat:@"%.06f", theAnnotation.coordinate.longitude];//longitude is a double here
	
    NSDictionary *annotation = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects: title, subtitle, latitude, longitude, nil] forKeys:[NSArray arrayWithObjects:@"title", @"subtitle", @"latitude", @"longitude", nil]];
	
	NSMutableArray *annotations = [[NSMutableArray alloc] init];
    if(NULL != [prefs objectForKey:@"annotations"])
	{
		annotations = [[prefs objectForKey:@"annotations"] mutableCopy];
	}
    [annotations addObject:annotation];
	NSLog(@"before storing Anno count: %d",[annotations count]);
	
    [prefs setObject:annotations forKey:@"annotations"];
	mapAnnotations = [annotations mutableCopy];
	
	[[NSUserDefaults standardUserDefaults] synchronize];
	[annotations release];
	[annotation release];
}

-(void)getUserPlaces
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	
	mapAnnotations = [[prefs objectForKey:@"annotations"] mutableCopy];
	
	NSLog(@"Stored Anno count: %d",[mapAnnotations count]);
	
	for (NSDictionary *annotation in mapAnnotations)
	{	
		MapPin *ann = [[MapPin alloc] init]; 
		ann.title = [annotation objectForKey:@"title"];
		ann.subtitle = [annotation objectForKey:@"subtitle"];
		CLLocationCoordinate2D point;
		point.longitude = [[annotation objectForKey:@"longitude"]doubleValue];
		point.latitude = [[annotation objectForKey:@"latitude"]doubleValue];
		ann.coordinate = point; 
		[mapView addAnnotation:ann];
		[ann release];
	}
}

-(void)deleteStoredPlace
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	
	mapAnnotations = [[prefs objectForKey:@"annotations"] mutableCopy];
	[mapAnnotations retain];
	
	NSLog(@"Stored Anno count: %d",[mapAnnotations count]);
	
	for(int i = 0; i < [mapAnnotations count]; i++){
		
		if ([self locationEqual:mapPin.coordinate.longitude isEqualTo:[[[mapAnnotations objectAtIndex:i] objectForKey:@"longitude"]doubleValue]]
			&& 	[self locationEqual:mapPin.coordinate.latitude isEqualTo:[[[mapAnnotations objectAtIndex:i] objectForKey:@"latitude"]doubleValue]])
		{
			[mapAnnotations removeObjectAtIndex:i];
			break;
		}
	}
	
	[prefs setObject:mapAnnotations forKey:@"annotations"];
	
	[[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)deletePlaceOnMap
{
	[mapView removeAnnotation:mapPin];
	//[mapView performSelectorOnMainThread:@selector(setNeedsDisplay) withObject:nil waitUntilDone:NO];
	[self deleteStoredPlace];
}


/****************************************
 Functions for user places table
 ***************************************/


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [mapAnnotations count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    static NSString *CellIdentifier = @"Cell";
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		cell.accessoryType =    UITableViewCellAccessoryDisclosureIndicator;
    }
	
	// Set up the cell...
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:18];

	NSDictionary *place = [mapAnnotations objectAtIndex:[indexPath row]];
	cell.textLabel.text = [place valueForKey:@"title"];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSDictionary *place = [mapAnnotations objectAtIndex:[indexPath row]];	
	currentCoordinates.longitude = [[place valueForKey:@"longitude"] doubleValue];
	currentCoordinates.latitude = [[place valueForKey:@"latitude"] doubleValue];
	placesTable.alpha = 0.0;
	MapPin *ann = [[MapPin alloc] init]; 
	ann.title = [place objectForKey:@"title"];
	ann.subtitle = [place objectForKey:@"subtitle"];
	CLLocationCoordinate2D point;
	point.latitude = [[place objectForKey:@"latitude"]doubleValue];
	point.longitude = [[place objectForKey:@"longitude"]doubleValue];
	ann.coordinate = point; 

	[self moveToLocation: ann];
	//[ann release];
	//[self sendPlace];
}

-(void)showViewAnimated:(UIView *)viewToAnimate{
	
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:1.0];
	[viewToAnimate setAlpha:0];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[viewToAnimate setAlpha:1];
	[UIView commitAnimations];
}


-(void)hideViewAnimated:(UIView *)viewToAnimate{
	
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:1.0];
	[viewToAnimate setAlpha:1];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
	[viewToAnimate setAlpha:0];
	[UIView commitAnimations];
}

-(IBAction)showPlacesTable
{
	if(0.0 == placesTable.alpha){
		[placesTable reloadData];
		//[placesTable performSelectorOnMainThread:@selector(setNeedsDisplay) withObject:nil waitUntilDone:NO];
		[self showViewAnimated:placesTable];
	}
	else {
		[self hideViewAnimated:placesTable];
	}
}


/****************************************
 Utility Functions
 ***************************************/

- (BOOL)locationEqual:(double)first isEqualTo:(double)second {
	
	if(fabsf(first - second) < kVerySmallValue)
        return YES;
	else
        return NO;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations.
	return (interfaceOrientation == UIInterfaceOrientationPortrait || UIInterfaceOrientationLandscapeLeft || UIInterfaceOrientationLandscapeRight);
}


- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
	// Release anything that's not essential, such as cached data
}


- (void)dealloc {
	//[locationManager release];
	//[location release];
	locationManager.delegate = nil;
	[mapAnnotations release];
	[mapView release];
	[self.mapView.userLocation removeObserver:self forKeyPath:@"location"];
    [self.mapView removeFromSuperview]; // release crashes app
    self.mapView = nil;
	[super dealloc];

}

@end
