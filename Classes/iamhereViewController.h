
#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "MapPin.h"


@interface iamhereViewController : UIViewController <MFMessageComposeViewControllerDelegate, 
													CLLocationManagerDelegate, MKMapViewDelegate, UITextFieldDelegate, UIAlertViewDelegate,
													UITableViewDelegate, UITableViewDataSource> 
{
	IBOutlet MKMapView *mapView;
	IBOutlet UITableView* placesTable;														
}

@property (nonatomic, retain) IBOutlet MKMapView *mapView;
@property (nonatomic, retain) UITableView* placesTable;

-(IBAction)sendIamhere;
-(IBAction)plotPin:(CLLocationCoordinate2D) point;
-(IBAction)trackUserLocation;
-(IBAction)showPlacesTable;
-(void)sendPlace;
-(void)moveToLocation:(MapPin *)newLocation;
-(void)updateMapWithLocation:(CLLocationCoordinate2D) newLocation;
-(void)showPlaceNameEntryAlert;
-(void)storeUsersPlace:(MapPin*) theAnnotation;
-(void)getUserPlaces;
-(void)deletePlaceOnMap;
-(BOOL)locationEqual:(double)first isEqualTo:(double)second;
@end

