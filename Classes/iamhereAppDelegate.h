
#import <UIKit/UIKit.h>

@class iamhereViewController;

@interface iamhereAppDelegate : NSObject <UIApplicationDelegate> {
	IBOutlet UIWindow *window;
	IBOutlet iamhereViewController *viewController;
}

@property (nonatomic, retain) UIWindow *window;
@property (nonatomic, retain) iamhereViewController *viewController;

@end

