//
//  GestureRecognizer.m
//  iamhere
//
//  Created by Andy on 14/05/2012.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "GestureRecognizer.h"


@implementation GestureRecognizer
@synthesize touchesBeganCallback;

-(id) init{
	if (self = [super init])
	{
		self.cancelsTouchesInView = NO;
	}
	return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	if (touchesBeganCallback)
		touchesBeganCallback(touches, event);
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
}

- (void)reset
{
}

- (void)ignoreTouch:(UITouch *)touch forEvent:(UIEvent *)event
{
}

- (BOOL)canBePreventedByGestureRecognizer:(UIGestureRecognizer *)preventingGestureRecognizer
{
	return NO;
}

- (BOOL)canPreventGestureRecognizer:(UIGestureRecognizer *)preventedGestureRecognizer
{
	return NO;
}

@end