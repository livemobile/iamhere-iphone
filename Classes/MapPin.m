
#import "MapPin.h"


@implementation MapPin

@synthesize title;
@synthesize subtitle;
@synthesize coordinate;

- (void)dealloc 
{
	[super dealloc];
	self.title = nil;
	self.subtitle = nil;
}
@end
